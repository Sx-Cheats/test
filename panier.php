<?php

$db = new PDO('mysql:host=localhost;dbname=siteweb_groupe;charset=utf8mb4', '', '');


function gen_uuid() {
  return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      // 32 bits for "time_low"
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

      // 16 bits for "time_mid"
      mt_rand( 0, 0xffff ),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand( 0, 0x0fff ) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand( 0, 0x3fff ) | 0x8000,

      // 48 bits for "node"
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
  );
};

$PANIER_USER_GET_ALL_ITEMS = function($id_user,$type='all')
{
  global $db;
  $items_user = $db->query("SELECT * FROM panier WHERE BINARY panier.iduser LIKE BINARY '$id_user' ");
  //if($type == 'all') 

  if($type == 'all') 
          return $items_user->fetchAll(PDO::FETCH_CLASS);
  else if ($type == 'iduser') 
        return $items_user->fetchAll(PDO::FETCH_COLUMN,0);
  else if ($type == 'idproduit')
        return $items_user->fetchAll(PDO::FETCH_COLUMN,1);
  else if ($type == 'nombre')
  return $items_user->fetchAll(PDO::FETCH_COLUMN,2);
  else if ($type=='date') 
      return $items_user->fetchAll(PDO::FETCH_COLUMN,3);
  else
   return false;

    
  return $items_user->fetchAll(PDO::FETCH_COLUMN&PDO::FETCH_CLASS ,0);
};
$PANIER_USER_GET_ONE_ITEM = function($id_user,$id_produit,$type='all')
{

  global $db;
  $data_panier =  $db->query("SELECT * FROM panier WHERE BINARY panier.iduser LIKE BINARY '$id_user' AND BINARY panier.idproduit LIKE BINARY '$id_produit'");
  $data_panier = $data_panier->fetchObject();
  if(empty($data_panier))
      return 0;
  if($type == 'all') 
        return  $data_panier;
  else if ($type == 'id user') 
        return $data_panier->iduser;
  else if ($type == 'id produit')
        return $data_panier->idproduit ;
  else if ($type == 'nombre')
       return $data_panier ->nombre ;
  else if ($type=='date') 
      return $data_panier->date;
  else if ($type=='prix')
     return $data_panier->prix;
  else
   return 0;

};

$PANIER_USER_INSERT_ITEM = function($iduser,$idproduit,$nombre,$prix,$Size,$auto_add_item=false)
{
  global $db;
    $date = date('G:i:s  j/m/Y ');
    $get_item = $db ->query("SELECT * FROM panier WHERE BINARY idproduit LIKE '$idproduit' ");
      if(!$get_item->rowCount())
      {
        $uiid = gen_uuid();
        $db->query("INSERT INTO panier(iduser, idproduit, nombre, date,Taille,prix,idcommand) VALUES('$iduser','$idproduit',$nombre,'$date','$Size','$prix','$uiid')");
        return 0;
      }else if($auto_add_item)
      {
        $get_item = $get_item->fetchObject(); //fetchAll -> table asoc sous un format string
        $add_length = $get_item->nombre+$nombre;
        $db->query("UPDATE panier SET panier.nombre=$add_length WHERE BINARY panier.iduser LIKE BINARY '$iduser' AND BINARY panier.idproduit LIKE BINARY '$idproduit'");
        return 1;
      }


};



$PANIER_USER_DELETE_ALL_ITEMS = function($iduser)
{
  global $db;
  $db->query("DELETE FROM panier WHERE BINARY panier.iduser LIKE BINARY '$iduser'");
};


$PANIER_USER_DELETE_ONE_ITEM = function($iduser,$idproduit)
{
  global $db;
  $db->query("DELETE FROM panier WHERE BINARY panier.iduser LIKE BINARY '$iduser'  AND BINARY panier.idproduit LIKE BINARY '$idproduit' ");
};
/*$PANIER_GET_PULL_SIZE = function ($idproduit)
{
  global $db;
  $Size_id = $db->query("SELECT produit_taille.idTaille FROM produit_taille WHERE BINARY produit_taille.idProduit LIKE BINARY '$idproduit'");
  if($Size_id->rowCount())
  {
    $Size_id = $Size_id->fetchObject()->idTaille;
    $Size = $db->query("SELECT * FROM taille WHERE BINARY taille.idTaille LIKE BINARY '$Size_id'");
    return $Size->fetchObject()->nom;
  }else
   return "Unique";
};*/

$GEN_HISTORY = function()
{
  global $db;
};


$PAYER = function($iduser)
{
  global $db;
  //$CLEAR_PANIER = $db->query("DELETE FROM panier WHERE BINARY panier.iduser LIKE BINARY '$iduser'");
  $data_panier_user = $db->query("SELECT * FROM panier WHERE BINARY panier.iduser LIKE BINARY '$iduser'");
  if($data_panier_user->rowCount())
  {
    $data_panier_user = $data_panier_user->fetchObject();
    $db->query("INSERT INTO command() VALUES()");
  }

  
};

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  if($_POST['DATA'])
  {
    $data = json_decode($_POST['DATA'],true);
    $type = $data['type'];
    if($type=='addpanier')
    {
     
    $idproduit_post = $data['idproduit'];
    $Size = $data['size'];

    $prix = ($db->query("SELECT prix FROM produit WHERE BINARY produit.idProduit LIKE BINARY '$idproduit_post'"))->fetchObject()->prix;
    $PANIER_USER_INSERT_ITEM($data['iduser'],$data['idproduit'],$data['nombre_de_produit'],$prix,$Size,true);
    }else if ($type=='loadpanier')
    {
      $user = $_COOKIE['JBF-Auth-Token'];
      $data_user_panier = $db->query("SELECT * from panier WHERE BINARY panier.iduser LIKE BINARY '$user'");
      if($data_user_panier->rowCount())
      {
        $user_produit_data = $data_user_panier->fetchAll();
        $data = [];
        foreach ($user_produit_data as $key => $value) {
          $v = $value['idproduit'];
          $item_dec = $db->query("SELECT * FROM produit WHERE BINARY produit.idProduit LIKE BINARY '$v'");
          array_push($data,$item_dec->fetchAll());
          array_push($data[$key],[$value['nombre'],$value['Taille']]);
        }
        
        print_r(json_encode(["status"=>1,'reponse'=> json_encode($data) ]));
      }else
      {
        print_r(json_encode(["status"=>0,'reponse'=>json_encode('Utilisateur introuvable')]));
      }
    }
  }
}

